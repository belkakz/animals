<?php 
$animals = [
	'africa' => ['Mammuthus columbi', 'Hemisotidae chukar', 'Hyperoliidae scolopacidae', 'Petropedetidae', 'Mantellidae'],
	'eurasia' => ['Mydaus javanensis', 'Alectoris chukar', 'Scolopacidae', 'Stercorarius parasiticus', 'Fulmarus'],
	'australia' => ['Tachyglossus', 'Notoryctemorphia', 'Monotremata', 'Myrmecobiidae', 'Vombatidae']
];
$twoNamesAnimals = [];
$continents = [];
$firstName = [];
$secondName = [];
foreach ($animals as $key => $value) {
	foreach ($value as $key2 => $value2) {
		$twoname = explode( " ", $value2);
		if (count($twoname) === 2) {
			$twoNamesAnimals[] = $value2; 
			$firstName[] = $twoname[0];
			$secondName[] = $twoname[1];
		}
		}
	}

shuffle($secondName);
$i = 0;
foreach ($firstName as $key => $value) {
			$fantasyAnimals[] = $value.' '.$secondName[$i];
			$i++;
	}
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Angry animals</title>
 </head>
 <body>
 	<style>
 		.allAnimals {
 			display: -webkit-flex;
 			display: -moz-flex;
 			display: -ms-flex;
 			display: -o-flex;
 			display: flex;
 		}
 		span {
 			color: red;
 		}
 	</style>
 	<h1>Жестокое обращение с животными</h1>
 	<h2>Список животных по континентам:</h2>
	<div class="allAnimals">
 	<?php 
 	foreach ($animals as $key => $value) {
 		 ?>
		<div class="block">

 		 <?php 
 		echo "<h3>$key</h3> <ul>";
 		foreach ($value as $key2 => $value2) {
 			echo "<li>$value2</li>";
 		}
 		echo '</ul>';
 		 ?>
 		</div>

 		 <?php 
 	}
 	echo '</div><hr>';

 	echo '<h2>Список животных с двойным названием:</h2><ul>';
 	foreach ($twoNamesAnimals as $key => $value) {
 		echo "<li>$value</li>";
 	}
 	echo '</ul>';

 	echo '<h2>Фантазийные животные</h2><ul>';
 	foreach ($fantasyAnimals as $key => $value) {
 		echo "<li><span>$value</span></li>";
 	}
 	echo '</ul>';
 	 ?>

 </body>
 </html>